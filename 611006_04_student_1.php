<?php
require_once 'vendor/autoload.php';

$doc_day = '26';
$doc_mon = 'สิงหาคม';
$doc_year = '2561';
$std_name = 'นายพรณพล ปุญญพันธุ์';
$std_id = '580611039';
$std_tel = '081-2345678';
$std_degree = 'ปริญญาโท';
$std_plan = '1.1 ฐานปริญญาโท';
$std_mjor = 'ระบบสารสนเทศและเครือข่าย';
$exam_date = '20 ต.ค. 2570';
$exam_time = '14:30 - 16:00';
$exam_place = 'RB5301';
$cmt = array('Ken Cosh', 'ดร. ยุทธพงษ์ สมจิต', 'ดร. สมภพ แก้วขวัญไกร');
$cmt_pst = array('ประธานกรรมการ', 'กรรมการ', 'กรรมการ');
$cm = 'Peter Parker';
$head = 'ผศ. ดร. ชาลล์ เซเวีย';
$exam_atp = 'ครั้งที่ 2';
$edu_term = '1';
$edu_year = '2561';

// Creating the new document..
$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0,'space' => array('line' => 'single')
);


$dec2 = array( 
    'align' => 'right','spaceBefore' => 120 , 'spaceAfter' => 0,'space' => array('line' => 'single')
);

$dec3 = array( 
    'align' => 'center','spaceBefore' => 120 , 'spaceAfter' => 0,'space' => array('line' => 'single')
);

$dec4 = array( 
    'align' => 'both','spaceBefore' => 240 , 'spaceAfter' => 0,'space' => array('line' => 'single')
);

$dec5 = array( 
    'align' => 'both', 'spaceAfter' => 0 ,'spaceBefore' => 0,'space' => array('line' => 'single')
);

$dec6 = array( 
    'spaceAfter' => 0 , 'indentation' => array('left' => 916),'space' => array('line' => 'single')
);

$dec7 = array( 
    'align' => 'center', 'spaceAfter' => 0,'spaceBefore' => 0, 'indentation' => array('left' => 3970),'space' => array('line' => 'single')
);

$dec8 = array( 
    'align' => 'left', 'spaceAfter' => 0 ,'spaceBefore' => 0,'space' => array('line' => 'single')
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 567)
  );

$section->addImage(
    'images/image1.png',
    array(
        'width'         => 98.05,
        'height'        => 85,
        'wrappingStyle' => 'infront',
        'align'=>'center'
    )
);

$section->addText(
    'แบบฟอร์มขอสอบวัดคุณสมบัติ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);

$section->addText(
    'คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec2 
);

$section->addText(
    'วันที่ '.
    $doc_day.
    ' เดือน '.
    $doc_mon.
    ' พ.ศ. '.
    $doc_year,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec2
);

$section->addText(
    htmlspecialchars("\t").
    'ข้าพเจ้า '.$std_name. 
    htmlspecialchars("\t").
    ' รหัสนักศึกษา '.$std_id.
    htmlspecialchars("\t").
    ' ระดับ '.$std_degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec4 
);

$section->addText( 
    'สาขาวิชา '.$std_mjor.
    htmlspecialchars("\t"). 
    $std_plan.
    htmlspecialchars("\t").
    'คณะวิศวกรรมศาสตร์',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5
);
$section->addText( 
    'โทรศัพท์ '.$std_tel.
    htmlspecialchars("\t"). 
    'ขอสอบวัดคุณสมบัติ ในวันที่ '.$exam_date.
    ' เวลา '.
    $exam_time,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5
);

$section->addText(
    'สถานที่สอบ '.$exam_place.
    '	รายชื่อกรรมการสอบ ดังนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5 
);

for($i = 0; $i < count($cmt); $i++) {
    $section->addText(
        ($i+1).'.  '.
        $cmt[$i]. 
        htmlspecialchars("\t\t\t").
        $cmt_pst[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec6  
    );
}

$section->addText(
    htmlspecialchars("\t").
    'จึงเรียนมาเพื่อโปรดพิจารณาและแต่งตั้งกรรมการสอบต่อไป',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec4  
);

$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5  
);

$section->addText(
    '(ลงนาม) ................................................นักศึกษา',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7
);

$section->addText(
    '('.$std_name.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7
);

$section->addText(
    htmlspecialchars("\t").
    '(โปรดระบุความเห็น).......................................................................................................................................................................................................................................................................'
    ,array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec8
);

$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);

$section->addText(
    '(ลงนาม) ................................................อาจารย์ที่ปรึกษา',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);

$section->addText(
    '('.$cmt[0].')',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7
);

$section->addText(
    '......... / ....................... / ...........',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);

$section->addText(
    htmlspecialchars("\t").
    '..........................................................................................................................................................................................................................................................................................................'
    ,array('name' => 'TH SarabunIT๙', 'size' => 16),$dec8 
);

$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5  
);

$section->addText(
    '(ลงนาม) ................................................ประธานฯ บศ.',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);

$section->addText(
    '('.$cm.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7
);

$section->addText(
    '......... / ....................... / ...........',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7
);

$section->addPageBreak();

$section->addImage(
    'images/image6.jpg',
    array(
        'width'         => 93,
        'height'        => 79.8,
        'wrappingStyle' => 'infront',
        'align'=>'center'
    )
);
$section->addText(
    'แบบสมัครสอบวัดคุณสมบัติ '.
    $exam_atp,
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);

$section->addText(
    htmlspecialchars("\t").
    'ข้าพเจ้า '.$std_name. 
    htmlspecialchars("\t").
    ' รหัสนักศึกษา '.$std_id.
    htmlspecialchars("\t").
    ' ระดับ '.$std_degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5 
);
$section->addText( 
    'สาขาวิชา '.$std_mjor.
    htmlspecialchars("\t").
    ' แบบ '. 
    $std_plan.
    htmlspecialchars("\t").
    'คณะวิศวกรรมศาสตร์',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5
);

$section->addText( 
    'โทรศัพท์ '.$std_tel.
    ' ขอสมัครสอบวัดคุณสมบัติ ประจำภาคการศึกษาที่ '.
    $edu_term.
    ' ปีการศึกษา '.
    $edu_year,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5  
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5  
);
$section->addText(
    '(ลงนาม) ................................................นักศึกษา',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);
$section->addText(
    '......... / ....................... / ...........',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec7 
);

$TableStyle1 = array( 'borderSize' => 6,'borderColor' => '000000', 'cellMargin' => 80 ,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
$table1 = $section->addTable($TableStyle1);
$table1->addRow();
$cell1 = $table1->addCell(5100);
$cell1 ->addText('สำหรับเจ้าหน้าที่สาขาวิชา', array('name' => 'TH SarabunIT๙', 'size' => 15, 'bold'=> true),$dec8);
$cell1 ->addText('1) กรณีค่าธรรมเนียมการศึกษารวมค่าสมัครสอบแล้ว', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '     ไม่ต้องชำระค่าสมัคร เนื่องจากเป็นการสอบครั้งแรก', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText('2) กรณีที่ต้องชำระค่าสมัครสอบ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '     ค่าธรรมเนียมการศึกษาไม่รวมค่าสมัครสอบ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสมัครสอบ 500 บาท (ป.โท)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสมัครสอบ 1,000 บาท (ป.เอก)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '     สอบครั้งแรกไม่ผ่าน ขอสอบแก้ตัวเป็นครั้งที่ ..........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสอบแก้ตัว 1,000 บาท (ป.โท/ปกติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสอบแก้ตัว 1,500 บาท (ป.โท/นานาชาติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสอบแก้ตัว 2,000 บาท (ป.เอก/ปกติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( '         ค่าสอบแก้ตัว 3,000 บาท (ป.เอก/นานาชาติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell1 ->addText( ' ', array('name' => 'TH SarabunIT๙', 'size' => 15),array('align' => 'center'),$dec5);
$cell1 ->addText('(ลงนาม) ......................................... เจ้าหน้าที่สาขา', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell1 ->addText('.......... / .............. / ...........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);

$cell2 = $table1->addCell(4900);
$cell2->addText('สำหรับเจ้าหน้าที่บัณฑิตวิทยาลัย ได้รับเงิน', array('name' => 'TH SarabunIT๙', 'size' => 15, 'bold'=> true),$dec8);
$cell2 ->addText('     ค่าสมัครสอบ 500 บาท (ป.โท)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( '     ค่าสมัครสอบ 1,000 บาท (ป.เอก)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText('     ค่าสอบแก้ตัว 1,000 บาท (ป.โท/ปกติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( '     ค่าสอบแก้ตัว 1,500 บาท (ป.โท/นานาชาติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( '         ค่าสอบแก้ตัว 2,000 บาท (ป.เอก/ปกติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( '         ค่าสอบแก้ตัว 3,000 บาท (ป.เอก/นานาชาติ)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( 'ตามใบเสร็จรับเงินเล่มที่ ................ เลขที่ ................... ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( 'วันที่ .......... / .............. / ...........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( 'จำนวนเงิน ................................. บาท', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec8);
$cell2 ->addText( ' ', array('name' => 'TH SarabunIT๙', 'size' => 16),$dec5);
$cell2 ->addText('(ลงนาม) ................................... เจ้าหน้าที่การเงิน', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell2 ->addText('.......... / .............. / ...........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);

$textrun = $section->createTextRun();
$textrun->addText(
    htmlspecialchars("\t").
    'หมายเหตุ  ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true),
    $dec8
);
$textrun->addText(
    ': ค่าสมัครสอบไม่สามารถรับคืนได้ ไม่ว่ากรณีใดๆ',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec8
);

$section->addLine(['weight' => 1, 'width' => 472.65, 'height' => 0]);

$section->addText(
    'ใบแจ้งผลการสอบวัดคุณสมบัติ '.
    $exam_atp,
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);

$section->addText(
    htmlspecialchars("\t").
    'ข้าพเจ้า '.$std_name. 
    htmlspecialchars("\t").
    ' รหัสนักศึกษา '.$std_id.
    htmlspecialchars("\t").
    ' ระดับ '.$std_degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5 
);
$section->addText( 
    'สาขาวิชา '.$std_mjor.
    htmlspecialchars("\t"). 
    $std_plan.
    htmlspecialchars("\t").
    'คณะวิศวกรรมศาสตร์',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec5
);

$TableStyle2 = array( 'borderSize' => 6 ,'borderColor' => '000000', 'cellMargin' => 80 ,'alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
$table2 = $section->addTable($TableStyle2);
$table2->addRow();
$cell1 = $table2->addCell();
$cell1 ->addText('การดำเนินการสอบเป็นไปตามข้อบังคับ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell1 ->addText('(ลงนาม) ........................... เจ้าหน้าที่ บว.', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell1 ->addText('(……………………………………)', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell1 ->addText('.......... / .............. / ...........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);

$cell2 = $table2->addCell();
$cell2->addText('ผลการประเมิน     S       U', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell2->addText('(ลงนาม)..............................ประธานฯ ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell2->addText('('.$head.')', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);
$cell2->addText('.......... / .............. / ...........', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec1);

$cell3 = $table2->addCell();
$cell3->addText('บันทึกอักษร S หรือ U', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec5);
$cell3->addText('ลงในช่องที่กำหนด', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec5);
$cell3->addText('หากแก้ไขให้ขีดฆ่าและ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec5);
$cell3->addText('ลงชื่อกำกับ', array('name' => 'TH SarabunIT๙', 'size' => 15),$dec5);

$textrun = $section->createTextRun();
$textrun->addText(
    htmlspecialchars("\t").
    'หมายเหตุ  ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true),
    $dec8
);
$textrun->addText(
    ': กรุณากรอกข้อมูลในใบสมัครสอบทั้งสองส่วน',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec8
);


// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_04_student_1.docx');

?>