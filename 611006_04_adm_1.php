<?php
require_once 'vendor/autoload.php';

$document_no = '                 /๒๕61';
$department = 'วิศวกรรมอุตสาหการ';
$degree = 'ปริญญาเอก';
$rule = 'ข้อ 23 ของข้อบังคับมหาวิทยาลัยเชียงใหม่ ว่าด้วยการศึกษาระดับบัณฑิตศึกษา พ.ศ.๒๕59 ลงวันที่ 25 สิงหาคม พ.ศ.2559 ';
$std_name = array('นางสาวเมธิดา ศิริตัน','นายนรพนธ์ วิเชียรสาร', 'นางสาวนิลุบล  ชลสวัสดิ์');
$std_id = array('590651003','590651003', '590651030');
$std_plan = array('แบบ 2.2 ฐาน ป.ตรี', 'แบบ 2.2 ฐาน ป.ตรี', 'แบบ 2.1 ฐาน ป.ตรี');
$std_exam_date = array('วันที่ 20 ธันวาคม พ.ศ. 2560','วันที่ 21 ธันวาคม พ.ศ. 2560','วันที่ 21 ธันวาคม พ.ศ. 2560 ');
$std_exam_time = array('10:30 – 12:00','14:30 – 16:00','14:30 – 16:00');
$std_exam_room = array('Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล','Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล','Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล');
$std_cmt_name = array(  array('รองศาสตราจารย์ ดร.วิมลิน เหล่าศิริถาวร','รองศาสตราจารย์ ดร.ศักดิ์เกษม ระมิงค์วงศ์','ผู้ช่วยศาสตราจารย์ ดร.รุ่งฉัตร ชมภูอินไหว','ผู้ช่วยศาสตราจารย์ ดร.อนิรุท ไชยจารุวณิช'),
                        array('รองศาสตราจารย์ ดร.คมกฤต เล็กสกุล','รองศาสตราจารย์ ดร.ธีรวรรณ บุญญวรรณ','รองศาสตราจารย์ ดร.วัสสนัย วรรธนัจฉริยา','ผู้ช่วยศาสตราจารย์ ดร.รุ่งฉัตร ชมภูอินไหว'),
                        array('รองศาสตราจารย์ ดร.คมกฤต เล็กสกุล','รองศาสตราจารย์ ดร.ธีรวรรณ บุญญวรรณ','รองศาสตราจารย์ ดร.วัสสนัย วรรธนัจฉริยา','ผู้ช่วยศาสตราจารย์ ดร.รุ่งฉัตร ชมภูอินไหว')
                    );
$std_cmt_pst = array(   array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ'),
                        array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ'),
                        array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ')
                    );
$document_start_date = '24 ตุลาคม พ.ศ.2560';
$document_date = '           กันยายน พ.ศ.๒๕60';
$head = 'ผู้ช่วยศาสตราจารย์ ดร.ณัฐ วรยศ';
$head_pst = 'คณบดีคณะวิศวกรรมศาสตร์';

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'center','spaceBefore' => 0, 'spaceAfter' => 0 
);

$dec2 = array( 
    'align' => 'both','spaceBefore' => 0 , 'spaceAfter' => 0
);

$dec3 = array( 
    'align' => 'both','spaceBefore' => 120 , 'spaceAfter' => 0
);

$dec4 = array( 
    'align' => 'both','spaceBefore' => 240 , 'spaceAfter' => 0
);

$dec5 = array( 
    'align' => 'center','spaceBefore' => 240 , 'spaceAfter' => 0, 'indentation' => array('left' => 3970, 'right' => 708)
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

$section->addImage(
    'images/image5.png',
    array(
        'width'         => 85,
        'height'        => 85,
        'wrappingStyle' => 'square',
        'align'=>'center'
    )
);

$section->addText(
    'คำสั่งคณะวิศวกรรมศาสตร์ มหาวิทยาลัยเชียงใหม่',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);
$section->addText(
    'ที่ '.
    $document_no,
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);
$section->addText(
    'เรื่อง   แต่งตั้งคณะกรรมการสอบวัดคุณสมบัตินักศึกษา'.
    $degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true) 
    ,$dec1
);
$section->addText(
    '--------------------------',
    array('name' => 'TH SarabunIT๙', 'size' => 16) 
    ,$dec1
);

$section->addText(
    htmlspecialchars("\t\t").
    'เพื่อให้การดำเนินการสอบวัดคุณสมบัติของนักศึกษาระดับปริญญาเอก สาขาวิชา'.
    $department.
    '  ดำเนินไปด้วยความเรียบร้อย',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec3 
);

$section->addText(
    htmlspecialchars("\t\t").
    'ฉะนั้น  อาศัยอำนาจตามความในมาตรา 40 แห่งพระราชบัญญัติมหาวิทยาลัยเชียงใหม่ พ.ศ. 2551 และตามความใน'.
    $rule.
    'จึงแต่งตั้งคณะกรรมการสอบวัดคุณสมบัติของนักศึกษาระดับ'.
    $degree.
    ' ดังนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16),
    $dec3
);

for($i = 0;$i< count($std_name); $i++){
    $section->addText(
        htmlspecialchars("\t\t").
        ($i+1).'.'.
        $std_name[$i].
        htmlspecialchars("\t").
        'รหัสนักศึกษา  '.
        $std_id[$i].
        htmlspecialchars("\t").
        $std_plan[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec2
    );
    $section->addText(
        htmlspecialchars("\t\t").
        'คณะกรรมการสอบวัดคุณสมบัติประกอบด้วย',
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec2
    );
    for($j=0; $j < count($std_cmt_name[$i]); $j++){
        $section->addText(
            htmlspecialchars("\t\t").
            ($j+1).'.'.
            $std_cmt_name[$i][$j].
            htmlspecialchars("\t\t").
            $std_cmt_pst[$i][$j],
            array('name' => 'TH SarabunIT๙', 'size' => 16),
            $dec2
        );
    }
    $section->addText(
        htmlspecialchars("\t\t").
        'โดยกำหนดสอบวันที่ '.
        $std_exam_date[$i].
        ' เวลา '.
        $std_exam_time[$i].
        ' น. ณ '.
        $std_exam_room[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec2
    );
    $section->addText(
        ' ',
        array('name' => 'TH SarabunIT๙', 'size' => 16 ),
        $dec2
    );
}

$section->addText(
    htmlspecialchars("\t\t").
    'ทั้งนี้  ตั้งแต่วันที่ '.
    $document_start_date.
    '  เป็นต้นไป',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec3
);

$section->addText(
    htmlspecialchars("\t\t\t").
    'สั่ง ณ วันที่'.
    htmlspecialchars("\t").
    $document_date,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    '('.$head.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);
$section->addText(
    $head_pst,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_04_adm_1.docx');