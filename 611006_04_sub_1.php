<?php
require_once 'vendor/autoload.php';

$dep = 'วิศวกรรมเครื่องกล';
$tel = 'โทร.42006 ต่อ 415';
$document_no = 'ศธ 6593(14).2/xxxx';
$document_date = '  สิงหาคม  ๒๕61';
$rev = array('รองศาสตราจารย์ ดร.พฤทธ์ สกุลช่างสัจจะทัย');
$rev_pst = array('ประธานกรรมการ');
$degree = 'ปริญญาเอก';
$std_dep = array(array('วิศวกรรมเครื่องกล', 'วิศวกรรมพลังงาน'));
$std_name = array(array(array('นายธนพล ยาฉาย','นางสาวเมธิดา ศิริตัน'),array('นางสาวสมหญิง มงคล')));
$std_id = array(array(array('600631023','590651003'), array('610632059')));
$std_plan = array(array(array('แบบ 2.2 ฐาน ป.ตรี','แบบ 1.1 ฐาน ป.โท'), array('แบบ 1.1 ฐาน ป.โท')));
$exam_date= 'วันพฤหัสบดีที่ 21 ธันวาคม พ.ศ. 2560';
$exam_time ='14:30 – 16:00';
$exam_room = 'Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล';
$head = 'รองศาสตราจารย์ ดร.ธงชัย ฟองสมุทร'; 
$head_pst = 'หัวหน้าภาควิชาวิศวกรรมเครื่องกล';

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'left', 'spaceBefore' => 0, 'spaceAfter' => 120 
);

$dec2 = array( 
    'align' => 'both', 'spaceBefore' => 0, 'spaceAfter' => 0 
);

$dec3 = array( 
    'align' => 'both', 'spaceBefore' => 240, 'spaceAfter' => 0 
);

$dec4 = array( 
    'align' => 'center', 'spaceBefore' => 0, 'spaceAfter' => 0 , 'indentation' => array('left' => 3402, 'right' => -2)
);

$dec5 = array( 
    'align' => 'both', 'spaceBefore' => 120, 'spaceAfter' => 0 
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

for($k = 0; $k<count($rev); $k++){
$std_count = 0;

for($i = 0; $i<count($std_dep[$k]); $i++) {
    $std_count += count($std_name[$k][$i]);
}

$textrun = $section->createTextRun($dec1);
$textrun->addImage(
    'images/image4.png',
    array(
        'width'         => 42.4,
        'height'        => 42.4,
        'align'         =>'left',
        'wrappingStyle' => 'square',
    )
);
$textrun->addText(
    htmlspecialchars("\t\t\t").
    'บันทึกข้อความ',
    array('name' => 'TH SarabunIT๙', 'size' => 29, 'bold'=>true) 
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'ส่วนงาน',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    'ภาควิชา'.
    $dep.
    '  คณะวิศวกรรมศาสตร์  ('.
    $tel.')     ',
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'ที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    '  '.$document_no.
    htmlspecialchars("\t\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);
$textrun->addText(
    'วันที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    $document_date.
    htmlspecialchars("\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'เรื่อง',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    '   ขอเรียนเชิญเข้าร่วมเป็น'.
    $rev_pst[$k].
    'สอบวัดคุณสมบัตินักศึกษาระดับ'.
    $degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec3);
$textrun->addText(
    'เรียน ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
);
$textrun->addText(
    $rev[$k],
    array('name' => 'TH SarabunIT๙', 'size' => 16)
);

$section->addText(
    htmlspecialchars("\t\t").
    'ตามที่ ภาควิชา'.
    $dep.
    'ได้กำหนดการสอบวัดคุณสมบัติ',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);

$section->addText(
    'ของนักศึกษาระดับ'.
    $degree.
    ' จำนวน '.
    $std_count.
    ' ราย ดังนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);

for($i = 0; $i < count($std_dep[$k]); $i++) {
    $section->addText(
        htmlspecialchars("\t\t").
        'สาขาวิชา'.
        $std_dep[$k][$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true ),
        $dec2
    );
    for($j = 0; $j <count($std_name[$k][$i]); $j++){
    $section->addText(
        htmlspecialchars("\t\t").
        ($j+1).
        '. '.
        $std_name[$k][$i][$j].
        htmlspecialchars("\t").
        ' รหัสนักศึกษา '.
        $std_id[$k][$i][$j].
        htmlspecialchars("\t").
        $std_plan[$k][$i][$j],
        array('name' => 'TH SarabunIT๙', 'size' => 16),
        $dec2
    );
    }
}
$section->addText(
    htmlspecialchars("\t\t").
    'ในการนี้ ภาควิชาฯ ใคร่ขอเรียนเชิญท่านเป็น'.
    $rev_pst[$k].
    'สอบวัดคุณสมบัติ',
    array('name' => 'TH SarabunIT๙', 'size' => 16,),
    $dec2
);
$section->addText(
    'ของนักศึกษา ดังกล่าว โดยกำหนดสอบใน'.
    $exam_date.
    ' เวลา '.
    $exam_time.
    'น. ณ '.
    $exam_room,
    array('name' => 'TH SarabunIT๙', 'size' => 16,),
    $dec2
);
$section->addText(
    htmlspecialchars("\t\t").
    'จึงเรียนมาเพื่อโปรดพิจารณา หากขัดข้องประการใดกรุณาแจ้งให้ภาควิชาฯ ทราบด้วยจักขอบคุณยิ่ง',
    array('name' => 'TH SarabunIT๙', 'size' => 16,),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    '('.$head.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);
$section->addText(
    $head_pst,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec4
);
if($k != count($rev)-1) {
    $section->addPageBreak();
}

}

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_04_sub_1.docx');
?>