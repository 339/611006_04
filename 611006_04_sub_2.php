<?php
require_once 'vendor/autoload.php';

$dep = 'วิศวกรรมเครื่องกล';
$tel = 'โทร.42006 ต่อ 415';
$document_no = 'ศธ 6593(14).2/xxxxxx';
$document_date = 'xx  สิงหาคม  ๒๕61';
$degree = 'ปริญญาเอก';
$std_dep = array('วิศวกรรมเครื่องกล','วิศวกรรมพลังงาน ');
$std_name = array(array('นางสาวเมธิดา ศิริตัน','นายธนพล ยาฉาย'),array('นางสาวสมหญิง มงคล'));
$std_id = array(array('590651003','610651002'),array('610632059'));
$std_plan = array(array('แบบ 2.2 ฐาน ป.ตรี','แบบ 1.1 ฐาน ป.โท'),array('แบบ 1.1 ฐาน ป.โท'));
$std_exam_date = array(array('วันที่ 20 ธันวาคม พ.ศ. 2560','วันที่ 21 ธันวาคม พ.ศ. 2560'),array('วันที่ 21 ธันวาคม พ.ศ. 2560 '));
$std_exam_time = array(array('10:30 – 12:00','14:30 – 16:00'),array('14:30 – 16:00'));
$std_exam_room = array(array('Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล','Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล'),array('Conference Room 1 อาคารเครื่องกล 2 ภาควิชาวิศวกรรมเครื่องกล'));
$std_cmt_name = array(  array(array('รองศาสตราจารย์ ดร.พฤทธ์ สกุลช่างสัจจะทัย','ผู้ช่วยศาสตราจารย์ ดร.นิติ คำเมืองลือ','ผู้ช่วยศาสตราจารย์ ดร.อารีย์ อัจฉริยวิริยะ'),
                        array('ศาสตราจารย์ ดร.ประดิษฐ์ เทอดทูล','รองศาสตราจารย์ ดร.พฤทธ์ สกุลช่างสัจจะทัย','ผู้ช่วยศาสตราจารย์ ดร.นิติ คำเมืองลือ','ผู้ช่วยศาสตราจารย์ ดร.อารีย์ อัจฉริยวิริยะ')),
                        array(array('ผู้ช่วยศาสตราจารย์ ดร.นิติ คำเมืองลือ','ศาสตราจารย์ ดร.ประดิษฐ์ เทอดทูล','รองศาสตราจารย์ ดร.พฤทธ์ สกุลช่างสัจจะทัย','ผู้ช่วยศาสตราจารย์ ดร.อารีย์ อัจฉริยวิริยะ'))
                    );
$std_cmt_pst = array(   array(array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ'),
                        array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ')),
                        array(array('ประธานกรรมการ','กรรมการ','กรรมการ','กรรมการ'))
                    );

$head = 'รองศาสตราจารย์ ดร.ธงชัย ฟองสมุทร';
$head_pst = 'หัวหน้าภาควิชาวิศวกรรมเครื่องกล';
$std_count = 0;

for($i = 0; $i<count($std_dep); $i++) {
    $std_count += count($std_name[$i]);
}

$phpWord = new \PhpOffice\PhpWord\PhpWord();

$dec1 = array( 
    'align' => 'left', 'spaceBefore' => 0, 'spaceAfter' => 120 
);

$dec2 = array( 
    'align' => 'both', 'spaceBefore' => 0, 'spaceAfter' => 0 
);

$dec3 = array( 
    'align' => 'both', 'spaceBefore' => 120, 'spaceAfter' => 0 
);

$dec4 = array( 
    'align' => 'both', 'spaceBefore' => 240, 'spaceAfter' => 0 
);

$dec5 = array( 
    'align' => 'center', 'spaceBefore' => 0, 'spaceAfter' => 0 , 'indentation' => array('left' => 3402, 'right' => -2)
);

$section = $phpWord->addSection(
    array('marginLeft' => 1701, 'marginRight' => 1134,
     'marginTop' => 851, 'marginBottom' => 851)
  );

$textrun = $section->createTextRun();
$textrun->addImage(
    'images/image4.png',
    array(
        'width'         => 42.4,
        'height'        => 42.4,
        'align'         =>'left',
        'wrappingStyle' => 'square'
    )
);
$textrun->addText(
    htmlspecialchars("\t\t\t\t").
    'บันทึกข้อความ',
    array('name' => 'TH SarabunIT๙', 'size' => 29, 'bold'=>true) 
    ,$dec1
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'ส่วนงาน',
    array('name' => 'TH SarabunIT๙', 'size' => 20, 'bold' => true)
);
$textrun->addText(
    htmlspecialchars("\t").
    'ภาควิชา'.
    $dep.
    '  คณะวิศวกรรมศาสตร์  ('.
    $tel.')     ',
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'ที่',
    array('name' => 'TH SarabunIT๙', 'size' => 20, 'bold' => true)
);
$textrun->addText(
    '  '.$document_no.
    htmlspecialchars("\t\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);
$textrun->addText(
    'วันที่',
    array('name' => 'TH SarabunIT๙', 'size' => 18, 'bold' => true)
);
$textrun->addText(
    $document_date.
    htmlspecialchars("\t\t"),
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec2);
$textrun->addText(
    'เรื่อง',
    array('name' => 'TH SarabunIT๙', 'size' => 20, 'bold' => true)
);
$textrun->addText(
    '   ขอความอนุเคราะห์ทำหนังสือแต่งตั้งคณะกรรมการสอบวัดคุณสมบัตินักศึกษาระดับ'.
   $degree,
    array('name' => 'TH SarabunIT๙', 'size' => 16,'underline' => 'dotted')
);

$textrun = $section->createTextRun($dec4);
$textrun->addText(
    'เรียน ',
    array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true)
);
$textrun->addText(
    'คณบดีคณะวิศวกรรมศาสตร์',
    array('name' => 'TH SarabunIT๙', 'size' => 16)
);

$section->addText(
    htmlspecialchars("\t").
    'ด้วยภาควิชา'.
    $dep.
    'มีความประสงค์จะขอแต่งตั้งคณะกรรมการสอบวัดคุณสมบัติของนักศึกษาระดับ'.
    $degree.
    ' จำนวน '.
    $std_count.
    ' ราย ดังนี้',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec3
);
for($i = 0; $i < count($std_dep); $i++) {
    $section->addText(
        htmlspecialchars("\t\t").
        'สาขาวิชา'.
        $std_dep[$i],
        array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true ),
        $dec3
    );
    for($j = 0; $j <count($std_name[$i]); $j++){
        $section->addText(
            htmlspecialchars("\t\t").
            ($j+1).
            '. '.
            $std_name[$i][$j].
            htmlspecialchars("\t").
            ' รหัสนักศึกษา '.
            $std_id[$i][$j].
            htmlspecialchars("\t").
            $std_plan[$i][$j],
            array('name' => 'TH SarabunIT๙', 'size' => 16, 'bold' => true ),
            $dec2
        );
        $section->addText(
            htmlspecialchars("\t\t").
            'คณะกรรมการสอบวัดคุณสมบัติประกอบด้วย',
            array('name' => 'TH SarabunIT๙', 'size' => 16),
            $dec2
        );
        for($k = 0; $k <count($std_cmt_name[$i][$j]); $k++){
            $section->addText(
                htmlspecialchars("\t\t").
                ($k+1).
                '. '.
                $std_cmt_name[$i][$j][$k].
                htmlspecialchars("\t").
                $std_cmt_pst[$i][$j][$k],
                array('name' => 'TH SarabunIT๙', 'size' => 16),
                $dec2
            );
        }
        $section->addText(
            htmlspecialchars("\t\t").
            'โดยกำหนดสอบใน'.
            $std_exam_date[$i][$j].
            ' เวลา '.
            $std_exam_time[$i][$j].
            ' น. ณ '.
            $std_exam_room[$i][$j],
            array('name' => 'TH SarabunIT๙', 'size' => 16),
            $dec2
        );
    }
}   
$section->addText(
    htmlspecialchars("\t\t").
    'จึงเรียนมาเพื่อโปรดพิจารณาดำเนินการต่อไป',
    array('name' => 'TH SarabunIT๙', 'size' => 16,),
    $dec3
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);
$section->addText(
    ' ',
    array('name' => 'TH SarabunIT๙', 'size' => 15 ),
    $dec2
);

$section->addText(
    '('.$head.')',
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);
$section->addText(
    $head_pst,
    array('name' => 'TH SarabunIT๙', 'size' => 16 ),
    $dec5
);

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('C:\xampp\htdocs\project192\resources\export_611006\611006_04_sub_2.docx');
?>